#pragma once
#include<string>
#include<time.h>
using namespace std;

class Player {
public:
	Player();
	Player(string name);
	string name;
	void introduction(string name);

	int pokemonInventorySize = 1;
	string pokemonInventory[6];

	int starterChoice;
	string starters[3]{ "Bulbasaur", "Charmander", "Squirtle" };
	int x = 0;
	int y = 0;
	int town = -1;
	int activity;
	char input;
	void move(int x, int y, int pokemonInventorySize, int town);
};