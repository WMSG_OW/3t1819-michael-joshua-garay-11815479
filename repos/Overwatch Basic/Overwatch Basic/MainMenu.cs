﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Overwatch_Basic
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Support supp = new Support();
            Hide();
            supp.ShowDialog();
        }

        private void DisplayAllbtn_Click(object sender, EventArgs e)
        {

        }

        private void Displaytnkbtn_Click(object sender, EventArgs e)
        {
            Tank tnk = new Tank();
            Hide();
            tnk.ShowDialog();
        }

        private void Displaydmgbtn_Click(object sender, EventArgs e)
        {
            Damage dmg = new Damage();
            Hide();
            dmg.ShowDialog();
        }

        private void DisplayAllbtn_Click_1(object sender, EventArgs e)
        {
            All all = new All();
            Hide();
            all.ShowDialog();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            StartScreen ss = new StartScreen();
            Hide();
            ss.ShowDialog();
        }
    }
}
