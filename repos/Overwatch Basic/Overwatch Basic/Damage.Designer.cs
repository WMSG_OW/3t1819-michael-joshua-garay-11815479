﻿namespace Overwatch_Basic
{
    partial class Damage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Damage));
            this.dgvAll = new System.Windows.Forms.DataGridView();
            this.Back = new System.Windows.Forms.Button();
            this.All = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.CloseMid = new System.Windows.Forms.Button();
            this.Mid = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.Long = new System.Windows.Forms.Button();
            this.Melee = new System.Windows.Forms.Button();
            this.clsmelee = new System.Windows.Forms.Button();
            this.midrngmelee = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAll
            // 
            this.dgvAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAll.Location = new System.Drawing.Point(523, 12);
            this.dgvAll.Name = "dgvAll";
            this.dgvAll.RowTemplate.Height = 24;
            this.dgvAll.Size = new System.Drawing.Size(369, 278);
            this.dgvAll.TabIndex = 0;
            this.dgvAll.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(755, 442);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(127, 38);
            this.Back.TabIndex = 1;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // All
            // 
            this.All.Location = new System.Drawing.Point(28, 376);
            this.All.Name = "All";
            this.All.Size = new System.Drawing.Size(96, 38);
            this.All.TabIndex = 2;
            this.All.Text = "All";
            this.All.UseVisualStyleBackColor = true;
            this.All.Click += new System.EventHandler(this.button1_Click);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(142, 376);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(162, 38);
            this.Close.TabIndex = 3;
            this.Close.Text = "Close Range";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // CloseMid
            // 
            this.CloseMid.Location = new System.Drawing.Point(329, 376);
            this.CloseMid.Name = "CloseMid";
            this.CloseMid.Size = new System.Drawing.Size(162, 38);
            this.CloseMid.TabIndex = 4;
            this.CloseMid.Text = "Close Mid Range";
            this.CloseMid.UseVisualStyleBackColor = true;
            this.CloseMid.Click += new System.EventHandler(this.CloseMid_Click);
            // 
            // Mid
            // 
            this.Mid.Location = new System.Drawing.Point(509, 376);
            this.Mid.Name = "Mid";
            this.Mid.Size = new System.Drawing.Size(96, 38);
            this.Mid.TabIndex = 5;
            this.Mid.Text = "Mid Range";
            this.Mid.UseVisualStyleBackColor = true;
            this.Mid.Click += new System.EventHandler(this.Mid_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(625, 376);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(143, 38);
            this.button5.TabIndex = 6;
            this.button5.Text = "Mid Long Range";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Long
            // 
            this.Long.Location = new System.Drawing.Point(787, 376);
            this.Long.Name = "Long";
            this.Long.Size = new System.Drawing.Size(96, 38);
            this.Long.TabIndex = 7;
            this.Long.Text = "Long Range";
            this.Long.UseVisualStyleBackColor = true;
            this.Long.Click += new System.EventHandler(this.Long_Click);
            // 
            // Melee
            // 
            this.Melee.Location = new System.Drawing.Point(28, 433);
            this.Melee.Name = "Melee";
            this.Melee.Size = new System.Drawing.Size(96, 40);
            this.Melee.TabIndex = 8;
            this.Melee.Text = "Melee";
            this.Melee.UseVisualStyleBackColor = true;
            this.Melee.Click += new System.EventHandler(this.Melee_Click);
            // 
            // clsmelee
            // 
            this.clsmelee.Location = new System.Drawing.Point(142, 433);
            this.clsmelee.Name = "clsmelee";
            this.clsmelee.Size = new System.Drawing.Size(162, 40);
            this.clsmelee.TabIndex = 9;
            this.clsmelee.Text = "Close Range/Melee";
            this.clsmelee.UseVisualStyleBackColor = true;
            this.clsmelee.Click += new System.EventHandler(this.clsmelee_Click);
            // 
            // midrngmelee
            // 
            this.midrngmelee.Location = new System.Drawing.Point(329, 433);
            this.midrngmelee.Name = "midrngmelee";
            this.midrngmelee.Size = new System.Drawing.Size(162, 40);
            this.midrngmelee.TabIndex = 10;
            this.midrngmelee.Text = "Mid Range/Melee";
            this.midrngmelee.UseVisualStyleBackColor = true;
            this.midrngmelee.Click += new System.EventHandler(this.midrngmelee_Click);
            // 
            // Damage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(894, 492);
            this.Controls.Add(this.midrngmelee);
            this.Controls.Add(this.clsmelee);
            this.Controls.Add(this.Melee);
            this.Controls.Add(this.Long);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.Mid);
            this.Controls.Add(this.CloseMid);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.All);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.dgvAll);
            this.Name = "Damage";
            this.Text = "Damage";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAll;
        private System.Windows.Forms.Button Back;
        private System.Windows.Forms.Button All;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button CloseMid;
        private System.Windows.Forms.Button Mid;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button Long;
        private System.Windows.Forms.Button Melee;
        private System.Windows.Forms.Button clsmelee;
        private System.Windows.Forms.Button midrngmelee;
    }
}