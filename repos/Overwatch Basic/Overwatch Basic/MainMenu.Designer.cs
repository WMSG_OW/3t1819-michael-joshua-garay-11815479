﻿namespace Overwatch_Basic
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.DisplayAllbtn = new System.Windows.Forms.Button();
            this.Displaydmgbtn = new System.Windows.Forms.Button();
            this.Displaytnkbtn = new System.Windows.Forms.Button();
            this.Displaysuppbtn = new System.Windows.Forms.Button();
            this.Back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DisplayAllbtn
            // 
            this.DisplayAllbtn.Location = new System.Drawing.Point(75, 66);
            this.DisplayAllbtn.Name = "DisplayAllbtn";
            this.DisplayAllbtn.Size = new System.Drawing.Size(274, 94);
            this.DisplayAllbtn.TabIndex = 0;
            this.DisplayAllbtn.Text = "DISPLAY ALL";
            this.DisplayAllbtn.UseVisualStyleBackColor = true;
            this.DisplayAllbtn.Click += new System.EventHandler(this.DisplayAllbtn_Click_1);
            // 
            // Displaydmgbtn
            // 
            this.Displaydmgbtn.Location = new System.Drawing.Point(75, 239);
            this.Displaydmgbtn.Name = "Displaydmgbtn";
            this.Displaydmgbtn.Size = new System.Drawing.Size(274, 102);
            this.Displaydmgbtn.TabIndex = 1;
            this.Displaydmgbtn.Text = "DISPLAY  DAMAGE HEROES";
            this.Displaydmgbtn.UseVisualStyleBackColor = true;
            this.Displaydmgbtn.Click += new System.EventHandler(this.Displaydmgbtn_Click);
            // 
            // Displaytnkbtn
            // 
            this.Displaytnkbtn.Location = new System.Drawing.Point(522, 66);
            this.Displaytnkbtn.Name = "Displaytnkbtn";
            this.Displaytnkbtn.Size = new System.Drawing.Size(233, 94);
            this.Displaytnkbtn.TabIndex = 2;
            this.Displaytnkbtn.Text = "DISPLAY TANK HEROES";
            this.Displaytnkbtn.UseVisualStyleBackColor = true;
            this.Displaytnkbtn.Click += new System.EventHandler(this.Displaytnkbtn_Click);
            // 
            // Displaysuppbtn
            // 
            this.Displaysuppbtn.Location = new System.Drawing.Point(522, 239);
            this.Displaysuppbtn.Name = "Displaysuppbtn";
            this.Displaysuppbtn.Size = new System.Drawing.Size(233, 102);
            this.Displaysuppbtn.TabIndex = 3;
            this.Displaysuppbtn.Text = "DISPLAY SUPPORT HEROES";
            this.Displaysuppbtn.UseVisualStyleBackColor = true;
            this.Displaysuppbtn.Click += new System.EventHandler(this.button4_Click);
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(379, 392);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(112, 46);
            this.Back.TabIndex = 4;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(919, 467);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.Displaysuppbtn);
            this.Controls.Add(this.Displaytnkbtn);
            this.Controls.Add(this.Displaydmgbtn);
            this.Controls.Add(this.DisplayAllbtn);
            this.Name = "MainMenu";
            this.Text = "MainMenu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button DisplayAllbtn;
        private System.Windows.Forms.Button Displaydmgbtn;
        private System.Windows.Forms.Button Displaytnkbtn;
        private System.Windows.Forms.Button Displaysuppbtn;
        private System.Windows.Forms.Button Back;
    }
}