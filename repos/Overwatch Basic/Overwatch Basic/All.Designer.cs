﻿namespace Overwatch_Basic
{
    partial class All
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(All));
            this.dgvAll = new System.Windows.Forms.DataGridView();
            this.Backbtn = new System.Windows.Forms.Button();
            this.Btnall = new System.Windows.Forms.Button();
            this.Supports = new System.Windows.Forms.Button();
            this.tankbtn = new System.Windows.Forms.Button();
            this.Onlydmg = new System.Windows.Forms.Button();
            this.unknownbtn = new System.Windows.Forms.Button();
            this.RealNameAbtn = new System.Windows.Forms.Button();
            this.realnameS = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAll
            // 
            this.dgvAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAll.Location = new System.Drawing.Point(78, 12);
            this.dgvAll.Name = "dgvAll";
            this.dgvAll.RowTemplate.Height = 24;
            this.dgvAll.Size = new System.Drawing.Size(472, 446);
            this.dgvAll.TabIndex = 0;
            this.dgvAll.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // Backbtn
            // 
            this.Backbtn.Location = new System.Drawing.Point(835, 412);
            this.Backbtn.Name = "Backbtn";
            this.Backbtn.Size = new System.Drawing.Size(126, 46);
            this.Backbtn.TabIndex = 1;
            this.Backbtn.Text = "Back";
            this.Backbtn.UseVisualStyleBackColor = true;
            this.Backbtn.Click += new System.EventHandler(this.Backbtn_Click);
            // 
            // Btnall
            // 
            this.Btnall.Location = new System.Drawing.Point(581, 41);
            this.Btnall.Name = "Btnall";
            this.Btnall.Size = new System.Drawing.Size(251, 38);
            this.Btnall.TabIndex = 5;
            this.Btnall.Text = "All";
            this.Btnall.UseCompatibleTextRendering = true;
            this.Btnall.UseVisualStyleBackColor = true;
            this.Btnall.Click += new System.EventHandler(this.Btnall_Click);
            // 
            // Supports
            // 
            this.Supports.Location = new System.Drawing.Point(581, 106);
            this.Supports.Name = "Supports";
            this.Supports.Size = new System.Drawing.Size(251, 38);
            this.Supports.TabIndex = 6;
            this.Supports.Text = "Only Supports";
            this.Supports.UseCompatibleTextRendering = true;
            this.Supports.UseVisualStyleBackColor = true;
            this.Supports.Click += new System.EventHandler(this.Supports_Click);
            // 
            // tankbtn
            // 
            this.tankbtn.Location = new System.Drawing.Point(581, 206);
            this.tankbtn.Name = "tankbtn";
            this.tankbtn.Size = new System.Drawing.Size(251, 38);
            this.tankbtn.TabIndex = 7;
            this.tankbtn.Text = "Only Tank";
            this.tankbtn.UseCompatibleTextRendering = true;
            this.tankbtn.UseVisualStyleBackColor = true;
            this.tankbtn.Click += new System.EventHandler(this.tankbtn_Click);
            // 
            // Onlydmg
            // 
            this.Onlydmg.Location = new System.Drawing.Point(581, 162);
            this.Onlydmg.Name = "Onlydmg";
            this.Onlydmg.Size = new System.Drawing.Size(251, 38);
            this.Onlydmg.TabIndex = 8;
            this.Onlydmg.Text = "Only Damage";
            this.Onlydmg.UseCompatibleTextRendering = true;
            this.Onlydmg.UseVisualStyleBackColor = true;
            this.Onlydmg.Click += new System.EventHandler(this.Onlydmg_Click);
            // 
            // unknownbtn
            // 
            this.unknownbtn.Location = new System.Drawing.Point(581, 373);
            this.unknownbtn.Name = "unknownbtn";
            this.unknownbtn.Size = new System.Drawing.Size(251, 38);
            this.unknownbtn.TabIndex = 9;
            this.unknownbtn.Text = "Real Name is Unknown";
            this.unknownbtn.UseCompatibleTextRendering = true;
            this.unknownbtn.UseVisualStyleBackColor = true;
            this.unknownbtn.Click += new System.EventHandler(this.unknownbtn_Click);
            // 
            // RealNameAbtn
            // 
            this.RealNameAbtn.Location = new System.Drawing.Point(581, 318);
            this.RealNameAbtn.Name = "RealNameAbtn";
            this.RealNameAbtn.Size = new System.Drawing.Size(251, 38);
            this.RealNameAbtn.TabIndex = 10;
            this.RealNameAbtn.Text = "Where Real Name Starts with A";
            this.RealNameAbtn.UseCompatibleTextRendering = true;
            this.RealNameAbtn.UseVisualStyleBackColor = true;
            this.RealNameAbtn.Click += new System.EventHandler(this.RealNameAbtn_Click);
            // 
            // realnameS
            // 
            this.realnameS.Location = new System.Drawing.Point(581, 262);
            this.realnameS.Name = "realnameS";
            this.realnameS.Size = new System.Drawing.Size(251, 38);
            this.realnameS.TabIndex = 11;
            this.realnameS.Text = "Where Real Name has S";
            this.realnameS.UseCompatibleTextRendering = true;
            this.realnameS.UseVisualStyleBackColor = true;
            this.realnameS.Click += new System.EventHandler(this.realnameS_Click);
            // 
            // All
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(973, 470);
            this.Controls.Add(this.realnameS);
            this.Controls.Add(this.RealNameAbtn);
            this.Controls.Add(this.unknownbtn);
            this.Controls.Add(this.Onlydmg);
            this.Controls.Add(this.tankbtn);
            this.Controls.Add(this.Supports);
            this.Controls.Add(this.Btnall);
            this.Controls.Add(this.Backbtn);
            this.Controls.Add(this.dgvAll);
            this.Name = "All";
            this.Text = "All";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAll;
        private System.Windows.Forms.Button Backbtn;
        private System.Windows.Forms.Button Btnall;
        private System.Windows.Forms.Button Supports;
        private System.Windows.Forms.Button tankbtn;
        private System.Windows.Forms.Button Onlydmg;
        private System.Windows.Forms.Button unknownbtn;
        private System.Windows.Forms.Button RealNameAbtn;
        private System.Windows.Forms.Button realnameS;
    }
}